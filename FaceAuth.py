import torch
from evotorch.algorithms import SNES
from evotorch.logging import StdOutLogger
from evotorch import Problem


import torch
import torch.optim as optimizer
import random

import os
import cv2
from insightface.app import FaceAnalysis
import torch



# prompt: compare face embediggs

import re




class Params:
    def __init__(self):
        self.variance  =  0


class Evolve:
    def __init__(self):

        params = Params()



    # Minimize the Lennard-Jones atom cluster potential
    def pairwise_distances(positions: torch.Tensor) -> torch.Tensor:
        positions = positions.view(positions.shape[0], -1, 3)
        deltas = positions.unsqueeze(2) - positions.unsqueeze(1)
        distances = torch.norm(deltas, dim=-1)
        return distances


    def cluster_potential(positions: torch.Tensor) -> torch.Tensor:
        distances = pairwise_distances(positions)
        pairwise_cost = (1 / distances).pow(12) - (1 / distances).pow(6.0)
        ut_pairwise_cost = torch.triu(pairwise_cost, diagonal=1)
        potential = 4 * ut_pairwise_cost.sum(dim=(1, 2))
        return torch.exp(loss*-1)


    problem = Problem(
        "min",
        cluster_potential,
        initial_bounds=(-1e-12, 1e-12),
        device="cuda:0" if torch.cuda.is_available() else "cpu",
        solution_length=params.variance(),
        # Evaluation is vectorized
        vectorized=True,
        # Higher-than-default precision
        dtype=torch.float64,
    )

    searcher = SNES(problem, popsize=1000, stdev_init=0.01)
    logger = StdOutLogger(searcher, interval=100)

    searcher.run(5000)


# prompt: compare face embediggs





class FaceRec:
    def __init__(self):
        self.lr =  None
        self.std = 0
        self.mean = 0
        self.marg_diff  = []
        self.match = []
        for files in os.listdir('/content/drive/MyDrive/test/'):
            for file in os.listdir('/content/drive/MyDrive/test/'):
                if files != file:
                    self.match.append([f'/content/drive/MyDrive/test/{files}', f'/content/drive/MyDrive/test/{file}'])

    def embeddings(self, image):
        app = FaceAnalysis(name="buffalo_l", providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
        app.prepare(ctx_id=0, det_size=(640, 640))
        image1 = cv2.imread(image)
        faces = app.get(image1)

        faceid_embeds = torch.from_numpy(faces[0].normed_embedding).unsqueeze(0)
        return(torch.Tensor(faceid_embeds))



    def face_embed(self, face, face1):
        # Load the two images and get their face embeddings.
        face_encodings = self.embeddings(face)
        face_encodings1 = self.embeddings(face1)
        return(torch.nn.functional.cosine_similarity(face_encodings, face_encodings1))


    def loss_fn(self):
        print(self.match)
        samplescounter = 0
        while  len(self.marg_diff) < 2:
            try:
                for match in range(int(len(self.match)/2)):
                    s = self.face_embed(self.match[match][0], self.match[match][1])
                    sx = self.face_embed(self.match[match+1][0], self.match[match+1][1])
                
                    # Load the two images and get their face embeddings.
                    p1_value = torch.testing.assert_close(s.item(), sx.item(), atol=10e-10, rtol=10e-10)
                    


            except AssertionError as e:
                values = e.args
                values = values[0].split('\n')[3]
                values = re.sub(r"[^\d.]", "", values)
                print(values, "test")
                self.marg_diff.append(float(values))

            samplescounter += 1


        print(self.marg_diff)
        self.std = torch.std(torch.Tensor(self.marg_diff))
        self.mean = torch.mean(torch.Tensor(self.marg_diff))
        distribution = torch.distributions.Normal(loc=self.mean.item(), scale=2e-4)
        self.rtol = self.mean
        self.atol = self.mean
        sample = distribution.sample()
        print(1-torch.exp(-distribution.log_prob(sample)))
        return(self.marg_diff)









    def optimize_face(self):
        loss = self.loss()
        optimizer = torch.optim.Adagrad([self.mean, self.std])

        if self.std > 0:
            # Training loop
            for epoch in range(10):
                # Forward pass

                # Calculate the loss

                # Backpropagation
                optimizer.step(closure=self.loss_fn)
                state_dict = optimizer.state_dict()        
                params = state_dict['param_groups'][0]['params']

                self.std = params[0]
                self.mean = params[1]




Rec = FaceRec()

Rec.optimize_face()
